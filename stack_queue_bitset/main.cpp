#include <iostream>
#include <stack>
#include <queue>
#include <cstdlib>
#include <bitset>

using namespace std;

int main()
{
    stack<int> s;
    queue<int> q;

    for(int i = 0; i < 10; ++i)
    {
        s.push(i);
        q.push(i);
    }


    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }

    cout << "\n";

    while (!q.empty())
    {
        cout << q.front() << " ";
        q.pop();
    }

    cout << "\n";

    priority_queue<int, vector<int>, greater<int> > pq;

    for(int i = 0; i < 10; ++i)
    {
        pq.push(rand() % 100);
    }


    while(!pq.empty())
    {
        cout << pq.top() << " ";
        pq.pop();
    }

    cout << "\n";

    bitset<16> bs1(248);

    bs1[0] = 1;
    bs1.flip(6);

    bitset<16> bs2(string("01010101011111"));

    bs2.flip();

    cout << "bs1: " << bs1 << endl;
    cout << "bs2: " << bs2 << endl;

    cout << "~(bs1 ^ bs2) = " << (~(bs1 ^ bs2) << 2) << endl;

    int value = bs2.to_ulong();
    string str = bs2.to_string();

    cout << value << " = " << str << endl;
}

