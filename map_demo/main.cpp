#include <iostream>
#include <map>

using namespace std;

template <typename Container>
void print(const Container& c, const string& desc)
{
    cout << desc << ": [ ";
    for(auto it = begin(c);
        it != end(c); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "(" << p.first << ", " << p.second << ")";
    return out;
}

int main()
{
    typedef map<int, string, greater<int> > DaysOfWeek;
    DaysOfWeek days_of_week = { { 1, "Pon" }, {2, "Wt" } };

    days_of_week.insert(DaysOfWeek::value_type(4, "Czw"));
    days_of_week.insert(pair<int, string>(6, "Sob"));
    days_of_week.insert(make_pair(7, "Nd"));

    auto where = days_of_week.find(6);

    if (where != days_of_week.end())
    {
        cout << where->second << endl;
    }

    days_of_week[3] = "Sr";

    cout << days_of_week[3] << endl;

    days_of_week.erase(3);

    print(days_of_week, "days_of_week");
}

