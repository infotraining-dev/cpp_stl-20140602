#include <iostream>
#include <unordered_set>
#include <boost/functional/hash.hpp>

using namespace std;

template <typename Unordered>
void unordered_desc(const Unordered& u)
{
    cout << "Size: " << u.size()
         << "; bucket_count: " << u.bucket_count()
         << "; max_load_factor: " << u.max_load_factor()
         << "; load_factor: " << u.load_factor() << endl;
}

struct Person
{
    string name;
    int age;

    Person(const string& name, int age) : name(name), age(age) {}

    bool operator==(const Person& p) const
    {
        return (p.name == name) && (p.age == age);
    }
};

namespace std
{
    template <>
    class hash<Person>
    {
    public:
        size_t operator()(const Person& p) const
        {
            hash<string> string_hasher;
            return string_hasher(p.name);
        }
    };
}

class PersonHash
{
public:
    size_t operator()(const Person& p) const
    {
        size_t hashed_value = 0;

        boost::hash_combine(hashed_value, p.name);
        boost::hash_combine(hashed_value, p.age);

        return hashed_value;
    }
};

int main()
{
    const int COUNT = 10000;

    unordered_set<int> uset1;

    //uset1.reserve(COUNT);

    //uset1.max_load_factor(8);

    unordered_desc(uset1);

   size_t previous_bucket_count = uset1.bucket_count();

    for(int i = 0; i < COUNT; ++i)
    {
        uset1.insert(rand() % COUNT);

        if (previous_bucket_count != uset1.bucket_count())
        {
            previous_bucket_count = uset1.bucket_count();
            cout << "Rehash to " << previous_bucket_count << "\n";
        }
    }

    unordered_desc(uset1);

    uset1.rehash(COUNT);

    unordered_desc(uset1);

//    for(size_t i = 0; i < uset1.bucket_count(); ++i)
//    {
//        cout << "Bucket " << i << ": [ ";
//        for(auto it = uset1.begin(i); it != uset1.end(i); ++it)
//            cout << *it << " ";
//        cout << "]\n";

//    }

    unordered_set<Person, PersonHash> people;

    people.insert(Person {"Kowalski", 44});
    people.insert(Person {"Nowak", 46});
    people.insert(Person {"Nijaki", 32});

    cout << "Count Kowalski 44: " << people.count(Person { "Kowalski", 44}) << endl;

}

