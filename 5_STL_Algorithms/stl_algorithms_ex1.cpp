#include "utils.hpp"

#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/if.hpp>


using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

void boost_lambda_demo()
{
    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    using namespace boost::lambda;

    for_each(vec.begin(), vec.end(),
             if_(_1 > 20)[cout << _1 << constant(">20"), cout << constant("\n")]);
    cout << "\n";
}

int main()
{
    boost_lambda_demo();

    using namespace placeholders;

    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec, "vec: ");

    // 1a - wyświetl parzyste
    cout << "Parzyste: ";
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            //bind(modulus<int>(), _1, 2));
            [](int x) { return x % 2 == 0; });
    cout << "\n";

    // 1b - wyswietl ile jest nieparzystych
    cout << "Ilosc nieparzystych: "
         <<  count_if(vec.begin(), vec.end(),
                      bind(modulus<int>(), _1, 2)) << endl;

    // 1c - wyswietl ile jest parzystych
    cout << "Ilosc parzystych: "
         <<  count_if(vec.begin(), vec.end(),
                      (bind(not2(modulus<int>()), _1, 2))) << endl;

    // 2 - usuń liczby podzielne przez 3
    int dzielnik = 3;

    vec.erase(remove_if(vec.begin(), vec.end(),
                        [=](int x) { return x % dzielnik == 0; }));

    print(vec, "bez podzielnych przez 3: ");

    // 3 - tranformacja: podnieś liczby do kwadratu
//    transform(vec.begin(), vec.end(), vec.begin(), vec.begin(),
//              multiplies<int>());

    double (*f)(double, double) = &pow;
    transform(vec.begin(), vec.end(), vec.begin(),
              bind(f, _1, 2.0));


    print(vec, "kwadraty: ");

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());
    cout << "5 najwiekszych: ";
    copy(vec.begin(), vec.begin() + 5, ostream_iterator<int>(cout, " "));
    cout << endl;

    // 5 - policz wartosc srednia
    //double avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
    double avg = 0.0;
    int count = 0;

    for_each(vec.begin(), vec.end(), [&](int x) { avg += x; count++; });

    avg /= count;

    cout << "avg: " << avg << endl;

    // 6 - usunąć duplikaty
    sort(vec.begin(), vec.end());
    vec.erase(unique(vec.begin(), vec.end()), vec.end());

    if (adjacent_find(vec.begin(), vec.end()) == vec.end())
        cout << "Brak duplikatow" << endl;

    print(vec, "bez duplikatow: ");
}
