#include <iostream>
#include <algorithm>
#include <tr1/array>

using namespace std;

void legacy_print(int* tab, size_t size)
{
    for(int* it = tab; it != tab + size; ++it)
        cout << *it << " ";
    cout << endl;
}

template <typename Container>
void stl_print(const Container& c)
{
    for(typename Container::const_iterator it = c.begin(); it != c.end(); ++it)
        cout << *it << " ";
    cout << endl;
}

int main()
{
    int tab1[10] = {};

    std::tr1::array<int, 10> arr1 = {1, 2, 3, 4, 4, 2, 0, 9};

    //legacy_print(tab1, 10);
    legacy_print(arr1.data(), arr1.size());

    stl_print(arr1);

    arr1.at(1) = 9;

    sort(arr1.begin(), arr1.end());

    stl_print(arr1);

    cout << "sizeof(arr1): " << sizeof(arr1) << endl;
}

