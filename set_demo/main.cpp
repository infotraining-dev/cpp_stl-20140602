#include <set>
#include <iostream>

using namespace std;

template <typename Container>
void print(const Container& c, const string& desc)
{
    cout << desc << ": [ ";
    for(auto it = begin(c);
        it != end(c); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

struct Person
{
    string name;
    int age;

    Person(const string& name, int age) : name(name), age(age) {}

    bool operator<(const Person& p) const
    {
        return name < p.name;
    }
};

class PersonByAgeComp
{
public:
    bool operator()(const Person& p1, const Person& p2) const
    {
        return p1.age < p2.age;
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(" << p.name << ", " << p.age << ")";
    return out;
}

int main()
{
    set<int> set_int1;

    set_int1.insert(8);
    set_int1.insert(2);
    set_int1.insert(4);
    set_int1.insert(9);
    set_int1.insert(5);

    print(set_int1, "set_int1");

    pair<set<int>::iterator, bool> result = set_int1.insert(5);

    if (result.second)
    {
        cout << *result.first << " został wstawiony\n";
    }
    else
        cout << *result.first << " juz jest w zbiorze\n";

    set<int>::iterator where = set_int1.find(9);

    if (where != set_int1.end())
        cout << "Znalazlem element " << *where << endl;

    multiset<Person, PersonByAgeComp> people;

    people.insert(Person("Kowalski", 44));
    people.insert(Person("Nowak", 45));
    people.insert(Person("Kowalski", 34));
    people.insert(Person("Anonim", 64));
    people.insert(Person("Nijaki", 44));
    people.insert(Person("Kowalski", 84));

    print(people, "people");

    auto range = people.equal_range(Person("Kowalski", 44));

    cout << "Kowalscy: ";
    for(auto it = range.first; it != range.second; ++it)
        cout << *it << " ";
    cout << endl;
}

