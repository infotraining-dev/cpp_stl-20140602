#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>

using namespace std;

void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

int main()
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    cout << "tab: ";
    for(int i = 0; i < 100; ++i)
        cout << tab[i] << " ";
    cout << "\n\n";

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    vector<int> vec_int(begin(tab), end(tab));

    // 2 - wypisz wartość średnią przechowywanych liczb w vec_int
    //avg(&vec_int[0], vec_int.size());
    avg(vec_int.data(), vec_int.size());

    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
    //vector<int> vec_cloned(vec_int);
    auto vec_cloned = vec_int;

    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
    vec_int.clear();
    vec_int.shrink_to_fit();

    int rest[] = { 1, 2, 3, 4 };
    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest
    vec_cloned.insert(vec_cloned.end(), begin(rest), end(rest));

    // 5 - posortuj zawartość wektora vec_cloned
    sort(vec_cloned.begin(), vec_cloned.end());

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów
    cout << "\nvec_cloned: ";
    for(vector<int>::const_iterator it = vec_cloned.begin();
        it != vec_cloned.end(); ++it)
        cout << *it << " ";
    cout << "\n\n";

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia) zawierający kopie elementów vec_cloned
    list<int> lst(vec_cloned.begin(), vec_cloned.end());

    // 8 - usuń duplikaty elementów przechowywanych w kontenerze
    lst.unique();

    // 9 - usuń liczby parzyste z kontenera
    lst.remove_if(&is_even);

    // 10 - wyświetl elementy kontenera w odwrotnej kolejności
    cout << "numbers: ";
    for(auto rit = lst.rbegin(); rit != lst.rend(); ++rit)
        cout << *rit << " ";
    cout << "\n\n";

    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers
    //list<int> lst_numbers(move(lst)); // C++11
    list<int> lst_numbers;
    lst_numbers.splice(lst_numbers.begin(), lst);

    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
    cout << "lst_numbers: ";
    for(auto rit = lst_numbers.rbegin(); rit != lst_numbers.rend(); ++rit)
        cout << *rit << " ";
    cout << "\n\n";
}

