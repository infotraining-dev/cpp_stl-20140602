#include <iostream>
#include <deque>

using namespace std;

template <typename Container>
void print(const Container& c, const string& desc)
{
    cout << desc << ": [ ";
    for(auto it = begin(c);
        it != end(c); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

int main()
{
    deque<string> words;

    words.push_front("One");
    words.push_front("Two");
    words.push_front("Three");
    words.push_back("Zero");

    print(words, "words");

    words.insert(words.begin() + 2, "New");

    for(int i = 0; i < words.size(); ++i)
        cout << words[i] << " ";
    cout << endl;
}

