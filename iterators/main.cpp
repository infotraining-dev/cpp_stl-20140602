#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <iterator>

using namespace std;

template <typename InIt, typename OutIt>
void m_copy(InIt start, InIt end, OutIt dest)
{
    while (start != end)
    {
        *(dest++) = *(start++);
    }
}

template <typename InIt, typename OutIt, typename Pred>
void m_copy_if(InIt start, InIt end, OutIt dest, Pred pred)
{
    while (start != end)
    {
        auto temp = *(start++);

        if (pred(temp))
            *(dest++) = temp;
    }
}

template <typename InIt>
typename std::iterator_traits<InIt>::value_type
    m_accumulate(InIt start, InIt end, typename std::iterator_traits<InIt>::value_type value)
{
    while(start != end)
    {
        value += *(start++);
    }

    return value;
}

inline bool is_even(int x)
{
    return x % 2 == 0;
}

class IsEven : public unary_function<int, bool>
{
public:
    bool operator()(int x) const
    {
        return x % 2 == 0;
    }
};

template <typename Container>
void print(const Container& c, const string& desc)
{
    cout << desc << ": [ ";
    for(auto it = begin(c);
        it != end(c); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

int main()
{
    list<int> lst1 = { 1, 2, 6, 3, 4, 7, 9 };

    vector<int> vec2;

    //back_insert_iterator<vector<int> > bii(vec2);

    m_copy_if(lst1.begin(), lst1.end(), back_inserter(vec2), not1(IsEven()));

    print(vec2, "vec2");

    list<int>::iterator it1 = lst1.begin();

    //it1 += 3;
    advance(it1, 3);

    cout << "*it1: " << *it1 << endl;

    list<int>::iterator it2 = lst1.end();
    advance(it2, - 1);

    cout << "*it2:" << *it2 << endl;

    cout << "Ilosc elementow: " << distance(it1, it2) << endl;

    int tab1[] = { 1, 2, 3, 4, 5 };

    cout << "sum of lst1: " << m_accumulate(lst1.begin(), lst1.end(), 0) << endl;
    cout << "sum of tab1: " << m_accumulate(tab1, tab1 + 5, 0) << endl;

    cout << "\nPodaj elementy:" << endl;

    istream_iterator<int> start(cin);
    istream_iterator<int> end;

    vector<int> vec3(start, end);

    cout << "vec3: ";
    m_copy(vec3.begin(), vec3.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
}

