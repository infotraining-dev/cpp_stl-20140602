#include <map>
#include <iostream>
#include <fstream>
#include <iterator>
#include <set>
#include <string>
#include <functional>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <unordered_map>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien
    danego slowa w pliku tekstowym.
    Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

typedef vector<string> WordsContainer;

WordsContainer load_words_from_file(const string& file_name)
{
    ifstream file_book(file_name);

    if (!file_book)
    {
        cout << "Blad otwarcia pliku..." << endl;
        exit(1);
    }

    string line;
    WordsContainer words;

    while (getline(file_book, line))
    {
        boost::tokenizer<> tok(line);
        words.insert(words.end(), tok.begin(), tok.end());
    }

    return words;
}

typedef map<string, int> Concordance;

typedef Concordance::value_type ConcordanceItem;

class CompareItemsByCountDesc
{
public:
    bool operator()(const ConcordanceItem* i1, const ConcordanceItem* i2) const
    {
        return i1->second > i2->second;
    }
};

typedef multiset<ConcordanceItem*, CompareItemsByCountDesc> Result;

int main()
{
    WordsContainer words = load_words_from_file("../holmes.txt");

    cout << "no of words: " << words.size() << endl;

    Concordance concordance;

    // sprawdzenie pisowni
    auto start = chrono::high_resolution_clock::now();

    // zliczenie wystapien
    for(auto& word : words)
        ++concordance[boost::to_lower_copy(word)];

    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed time: "
         << chrono::duration_cast<chrono::milliseconds>(end-start).count()
         << " ms" << endl;

    // wyswietlenie 10 najczesciej wystepujacych slow
    Result result;

    for(Concordance::iterator it = concordance.begin();
        it != concordance.end(); ++it)
    {
        result.insert(&(*it));
    }

    int count = 0;
    for(Result::iterator it = result.begin();
        it != result.end(); ++it, ++count)
    {
        if (count == 10)
            break;

        cout << (*it)->second << " - " << (*it)->first << endl;
    }

    cout << "\nUnique words:";

    ConcordanceItem key("", 1);
    auto range_with_unique_words = result.equal_range(&key);

    for(auto it = range_with_unique_words.first; it != range_with_unique_words.second; ++it)
        cout << (*it)->first << endl;
}
