#include <iostream>
#include <fstream>
#include <set>
#include <unordered_set>
#include <string>
#include <chrono>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <chrono>

using namespace std;

class CaseInsensitiveStringComp
{
public:
    bool operator()(const string& str1, const string& str2) const
    {
        return
            boost::to_lower_copy(str1)
                < boost::to_lower_copy(str2);
    }
};

class CaseInsensitiveStringEq
{
public:
    bool operator()(const string& str1, const string& str2) const
    {
        return
            boost::to_lower_copy(str1) == boost::to_lower_copy(str2);
    }
};

class CaseInsesitiveStringHash
{
public:
    size_t operator()(const string& str) const
    {
        string lowered_str = boost::to_lower_copy(str);
        std::hash<string> string_hasher;
        return string_hasher(lowered_str);
    }
};

int main()
{
     // wszytaj zawartość pliku en.dict ("słownik języka angielskieog")
     // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list;

    string input_text = "To Sherlock Holmes she is always the woman. I have seldom heard "
                        "him mention her under any other name. In his eyes she eclipses "
                        "and predominatds the whole of her sex. It was not that he felt "
                        "any emotion akdin to love for Irene Adler. All emotions, and that "
                        "one particularly, were abhorrent to his cold, precise but "
                        "admirably balanced mind "
                        "this is an exmple of snetence";

    boost::tokenizer<> tok(input_text);

    ifstream file_in("../en.dict");

    if (!file_in)
        throw runtime_error("File not found!");

    auto start_time = chrono::high_resolution_clock::now();

    istream_iterator<string> start(file_in);
    istream_iterator<string> end;

    set<string, CaseInsensitiveStringComp> dictionary(start, end);
    //unordered_set<string, CaseInsesitiveStringHash, CaseInsensitiveStringEq> dictionary;

    cout << "Dictionary size: " << dictionary.size() << endl;

    for(auto word : tok)
    {
        if (!dictionary.count(word))
            cout << "Error: " << word << endl;
    }

    auto end_time = chrono::high_resolution_clock::now();

    cout << "Elapsed time: " << chrono::duration_cast<chrono::milliseconds>(end_time - start_time).count() << endl;
}

