#include <iostream>
#include <list>

using namespace std;

template <typename Container>
void print(const Container& c, const string& desc)
{
    cout << desc << ": [ ";
    for(auto it = begin(c);
        it != end(c); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}


int main()
{
    list<int> lst1 = { 1, 2, 3, 7, 2, 4, 5 };
    list<int> lst2 = { 5, 4, 3, 3, 6, 5, 5, 5, 9 };

    print(lst1, "lst1");
    print(lst2, "lst2");

    auto it1 = lst1.begin();
    ++it1;

    auto it2 = lst1.end();
    --it2;

    lst2.splice(++lst2.begin(), lst1, it1, it2);

    cout << "\npo splice:\n";

    print(lst1, "lst1");
    print(lst2, "lst2");

    lst1.sort();
    lst2.sort();

    cout << "\npo sort:\n";

    print(lst1, "lst1");
    print(lst2, "lst2");

    lst1.merge(lst2);

    cout << "\npo merge:\n";

    print(lst1, "lst1");
    print(lst2, "lst2");

    lst1.remove(3);
    print(lst1, "lst1");
}

