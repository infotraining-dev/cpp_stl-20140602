#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <iterator>
#include <memory>
#include <functional>
#include <string>

using namespace std;

inline bool is_even(int x)
{
    return x % 2 == 0;
}

class IsEven : public unary_function<int, bool>
{
public:
    bool operator()(int x) const
    {
        return x % 2 == 0;
    }
};

template <typename Container>
void print(const Container& c, const string& desc)
{
    cout << desc << ": [ ";
    for(auto it = begin(c);
        it != end(c); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

struct Person
{
    string name;
    int age;

    bool is_retired() const
    {
        return age > 67;
    }

    void print_in_stream(ostream& out, const string& prefix) const
    {
        out << prefix << "name: " << name << "; age: " << age << endl;
    }

    string to_string(const string& prefix) const
    {
        return prefix + " name: " + name + ", age: " + std::to_string(age);
    }

    Person(const string& name, int age) : name(name), age(age) {}
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(" << p.name << ", " << p.age << ")";
    return out;
}

int main()
{
    vector<int> vec1 = { 1, 2, 3, 5, 9, 2, 4, 7 };
    vector<int> vec2;

    remove_copy_if(vec1.begin(), vec1.end(), back_inserter(vec2), IsEven());

    print(vec1, "vec1");
    print(vec2, "vec2");

    vector<Person> people = { Person { "Kowalski", 45 }, Person { "Nowak", 70 },
                              Person { "Niajki", 75 }, Person { "Anonim", 33 } };

    vector<Person*> people_ptrs = { &people[0], &people[1], &people[2], &people[3] };

    vector<Person> retired;

    // C++11
    copy_if(people.begin(), people.end(), back_inserter(retired),
            mem_fn(&Person::is_retired));

    print(retired, "retired");

    vector<Person*> retired_ptrs;

    copy_if(people_ptrs.begin(), people_ptrs.end(), back_inserter(retired_ptrs),
            //mem_fn(&Person::is_retired));
            bind(&Person::is_retired, placeholders::_1));

    cout << "retired_ptrs: ";
    for(auto ptr : retired_ptrs)
        cout << *ptr << " ";
    cout << endl;

    vector<shared_ptr<Person>> people_shared =
        {
            make_shared<Person>("Kowalski", 55),
            make_shared<Person>("Nowak", 82),
            make_shared<Person>("Nowacki", 77)
        };

    decltype(people_shared) retired_shared;

    copy_if(people_shared.begin(), people_shared.end(), back_inserter(retired_shared),
            mem_fn(&Person::is_retired));

    cout << "retired_shared: ";
    for(auto ptr : retired_shared)
        cout << *ptr << " ";
    cout << endl;

    vector<int> numbers =  { 5, 87, 32, 564, 7768, 23 , 43, 6564 };

    greater<int> gt;

    cout << "30>15: " << gt(30, 15) << endl;

    using namespace placeholders;

    auto where = find_if(numbers.begin(), numbers.end(),
                         //bind1st(less_equal<int>(), 100));
                         bind(less_equal<int>(), 100, _1));

    if (where != numbers.end())
        cout << "Pierwsza > 100: " << *where << endl;

    cout << "Print:\n";
    for_each(people.begin(), people.end(),
             bind(&Person::print_in_stream, _1, ref(cout), "Osoba - "));

    //auto f = bind(&Person::print_in_stream, _1, ref(cout), "Osoba - ");

    //Person p { "Nowak", "88" };

    //f(p);  // p.print_in_stream(cout, "Osoba - ");

    vector<string> prefixes = { "Pierwszy - ", "Drugi - ", "Trzeci - ", "Czwarty - " };


    transform(people.begin(), people.end(),
              prefixes.begin(),
              ostream_iterator<string>(cout, "\n"),
              //bind(&Person::to_string, _1, _2));
              [](const Person& p, const string& prefix) { return p.to_string(prefix); }
    );

    vector<int> vec3 = { 1, 2, 3, 4, 2, 3, 4, 2, 5, 7, 2, 2, 4 };

    print(vec3, "vec3");

    auto it = partition(vec3.begin(), vec3.end(), IsEven());

    copy(it, vec3.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";

    print(vec3, "vec3");

    sort(vec3.begin(), vec3.end());

    print(vec3, "vec3");

    //remove(vec3.begin(), vec3.end(), 2);
    //vec3.erase(remove(vec3.begin(), vec3.end(), 2), vec3.end());
    vec3.erase(unique(vec3.begin(), vec3.end()), vec3.end());


    print(vec3, "vec3");
}

