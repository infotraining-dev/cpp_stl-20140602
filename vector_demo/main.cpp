#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <memory>

using namespace std;

//class ListIterator
//{
//    Node* current_;

//public:
//    ListIterator& operator++()  // ++it, ++++it;
//    {
//        current_ = current_->next;
//        return *this;
//    }

//    const ListIterator operator++(int) // it++++
//    {
//        ListIterator temp(*this);

//        current_->next;
//        return temp;
//    }
//};

template <typename T>
void vec_desc(const vector<T>& vec, const string& desc)
{
    cout << desc << " - size: " << vec.size()
         << "; capacity: " << vec.capacity()
         << "; max_size: " << vec.max_size() << endl;
}

template <typename Container>
void print(const Container& c, const string& desc)
{
    cout << desc << ": [ ";
    for(auto it = begin(c);
        it != end(c); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

struct Person
{
    string name;
    int age;

    Person(const string& name, int age) : name(name), age(age) {}
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(" << p.name << ", " << p.age << ")";
    return out;
}

int main()
{
    vector<int> vec_int1;
    vector<string> vec_string;

    vec_desc(vec_int1, "vec_int1");
    vec_desc(vec_string, "vec_string");

    vector<int> vec_int2(10);
    vec_desc(vec_int2, "vec_int2");

    // C++11
    for(auto item : vec_int2)
        cout << item << " ";
    cout << endl;

    vector<string> words = { "One", "Two", "Three" };

    print(words, "words");

    for(auto& item : words)
        cout << item;
    cout << endl;

    print(words, "words");

    print(vec_int2, "vec_int2");

    list<int> lst_int1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    print(lst_int1, "lst_int1");

    int tab1[10] = { 1, 2, 3, 4, 5, 6 };

    print(tab1, "tab1");

    //vector<int> vec_int3(tab1, tab1 + 10);
    vector<int> vec_int3(begin(tab1), end(tab1));

    print(vec_int3, "vec_int3");

    vec_int3.at(9) = 10;

    print(vec_int3, "vec_int3");

    for(vector<int>::const_reverse_iterator it = vec_int3.rbegin();
        it != vec_int3.rend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    vector<int> vec_int4(vec_int3.rbegin(), vec_int3.rend());
    print(vec_int4, "vec_int4");

    vector<int> vec_int5;
    size_t vec_capacity = vec_int5.capacity();

    vec_int5.reserve(10000000);

    vec_desc(vec_int5, "vec_int5");

    for(int i = 0; i < 1000000; ++i)
    {
        vec_int5.push_back(i);
        if (vec_capacity != vec_int5.capacity())
        {
            vec_capacity = vec_int5.capacity();
            cout << "reallocation to new capcity: " << vec_capacity << endl;
        }
    }

    vec_int5.resize(32);
    //vec_int5.shrink_to_fit(); // C++11
    vector<int>(vec_int5).swap(vec_int5); // C++03
    vec_desc(vec_int5, "vec_int5");

    cout << endl;
    print(words, "words");

    vector<string> new_words = { "New1", "New2", "New3" };
    vector<string>::iterator where = words.begin() + 1;
    //words.insert(where, "New");
    words.insert(where, new_words.begin(), new_words.end());

// unieważnienie iteratora w pętli for!!!
//    for(vector<string>::iterator it = words.begin();
//        it != words.end(); ++it)
//    {
//        if (*it == "Three")
//            words.erase(it);
//    }

    print(words, "words");

    words.assign(new_words.begin(), new_words.end());

    words.reserve(16);

    vec_desc(words, "words");

    vector<Person> people;

    Person p("Jan", 34);
    people.push_back(std::move(p));  // push_back(const string&);
    people.push_back(Person("Adam", 55));  // push_back(string&&);
    people.emplace_back("Ewa", 44);

    print(people, "people");

    cout << "po przesunieciu: " << p << endl;

    vector<unique_ptr<Person>> ptr_people;

    unique_ptr<Person> ptr1(new Person("Adam", 45));
    ptr_people.push_back(move(ptr1));
    ptr_people.push_back(unique_ptr<Person>(new Person("Jerzy", 33)));
    ptr_people.emplace_back(new Person("Anna", 22));

    for(auto& p : ptr_people)
        cout << *p << endl;

    vector<bool> vec_bool = { true, false, false, true, true };

    vec_bool[2].flip();

    print(vec_bool, "vec_bool");

    vec_bool.flip();

    print(vec_bool, "vec_bool");

    bool* ptr_bool = &vec_bool[0];
}

